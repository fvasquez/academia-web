<?php
    include_once 'funciones.php';
?>

<html>
    <head>
        <meta charset="UTF-8" />
        <script src="./js/jquery-2.1.1.min.js"></script>
        <script src="./js/formulario.js"></script>
        <link  rel="stylesheet" href="css/formulario.css" type="text/css" />
    </head>
    <body>
        <?php if(isset($_POST['usuario'])){
            echo "<p>Bienvenido ".$_POST['usuario']. " Tu contraseña es ". $_POST['password']."</p>";
        }
        else{?>
        <form method="POST">
            <label for="usuario">Usuario:</label>
            <input type="text" id="usuario" name="usuario" />
            <label for="password">Contraseña:</label>
            <input type="password" id="password" name="password" />
            <input type="submit" value="Enviar" />
        </form>
        <?php } ?>
    </body>
</html>

