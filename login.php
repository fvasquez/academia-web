<?php
    require_once './Clases/Usuario.php';
?>

<html>
    <head>
        <meta charset="UTF-8" />
        <script src="./js/jquery-2.1.1.min.js"></script>
        <script src="./js/formulario.js"></script>
        <link  rel="stylesheet" href="css/formulario.css" type="text/css" />
    </head>
    <body>
        <?php if(isset($_POST['usuario'],$_POST['password']) && !empty($_POST['usuario'])  && !empty($_POST['password'])){
            $username = $_POST['usuario'];
            $password = $_POST['password'];
            
            $u = new Usuario($username, $password);
            
            if($u->login())
                echo "<p>Bienvenido</p>";
            else
                    echo "<p>Los datos que ingreso son incorrectos. Intente nuevamente <a href='./login.php'>aqui</a></p>";
        }
        else{?>
        <form method="POST">
            <label for="usuario">Usuario:</label>
            <input type="text" id="usuario" name="usuario" />
            <label for="password">Contraseña:</label>
            <input type="password" id="password" name="password" />
            <input type="submit" value="Enviar" />
        </form>
        <?php } ?>
    </body>
</html>

