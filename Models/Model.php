<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author fvasquez
 */
class Model {
    
    public function primaryKey()
    {
        return "id";
    }
    
    public function save(){
        
        $array = get_object_vars($this);
        $columns = "";
        $values = "";
        foreach ($array as $key => $value) {
            if($key == "id")
                continue;
            if($columns == ""){
                $columns = $key;                
                $values = ($value == null) ? "NULL" : "'".$value."'";
            }
            else{
                $columns = $columns .",".$key;
                
                $values = ($value == null) ? $values. ", NULL" : $values . "," .  "'".$value."'";
            }
        }
        $query = "INSERT INTO ". get_class($this) ."(".$columns.") VALUES (".$values.")";
        
        
        $con = mysqli_connect("localhost","root","","academia_web");
        
        $result = $con->query($query);

        return($result > 0);
    }
    public function delete(){
        $attributes = get_object_vars($this);
        $query = "DELETE FROM ". get_class($this) ." WHERE ".$this->primaryKey()."='". $attributes[$this->primaryKey()]."'";
        
        $con = mysqli_connect("localhost","root","","academia_web");
        
        $result = $con->query($query);
        
        return($result > 0);
    }
    public function findAll(){
        $query = "SELECT * FROM ". get_class($this);
        
        $con = mysqli_connect("localhost","root","","academia_web");
        
        $result = $con->query($query);
        
        $models = array();
        
        while($row = mysqli_fetch_array($result)) {
            $nombre = get_class($this);
            $m = new $nombre;
            foreach ($row as $key => $value) {
                $m->$key = $value;
            }
            $models[] = $m;
        } 
        return $models;
    }
    public function findByPk($id){
        
    }
    
}
