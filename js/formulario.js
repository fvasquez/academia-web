/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    $("#usuario").blur(function(){
        if(!$("#usuario").val())
        {
            $("#usuario").addClass("error");
        }
        else{
            $("#usuario").removeClass("error");
        }
    });
    $("#password").blur(function(){
        if($("#password").val() == "")
        {
            $("#password").addClass("error");
        }
        else{
            $("#password").removeClass("error");
        }
    });
});
