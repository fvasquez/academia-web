<?php
    require_once './Clases/PasswordHash.php';
    require_once './Clases/Usuario.php';
?>

<html>
    <head>
        <meta charset="UTF-8" />
        <script src="./js/jquery-2.1.1.min.js"></script>
        <script src="./js/formulario.js"></script>
        <link  rel="stylesheet" href="css/formulario.css" type="text/css" />
    </head>
    <body>
        <?php if(isset($_POST['usuario'],$_POST['password']) && !empty($_POST['usuario'])  && !empty($_POST['password'])){
            $username = $_POST['usuario'];
            $password = create_hash($_POST['password']);
            
            $u = new Usuario($username, $password);
            
            if($u->save())
            {
                echo "<p>Usuario registrado exitosamente</p>";
            }
            else
                echo "<p>Usuario no registrado</p>";
        }
        else{?>
        <form method="POST">
            <label for="usuario">Usuario:</label>
            <input type="text" id="usuario" name="usuario" />
            <label for="password">Contraseña:</label>
            <input type="password" id="password" name="password" />
            <input type="submit" value="Enviar" />
        </form>
        <?php } ?>
    </body>
</html>

